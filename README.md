# DielectricShell_JIMSS2024

To run the python scripts, install Netgen/NGSolve:

Depending on platform, use either pip (windows) or pip3 (linux/macos):
> pip install --upgrade ngsolve

to work with jupyter notebooks, additionally install jupyter widgets
> pip install jupyter
> pip install --upgrade webgui_jupyter_widgets


see also ngsolve.org/downloads

