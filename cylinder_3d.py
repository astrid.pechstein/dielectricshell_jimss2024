#%%
import netgen.gui
from ngsolve import *

import numpy as np

from netgen.occ import *

from newtonmethod import NewtonWithLinesearch

SetNumThreads(4)
#%%

## choose one
use_Efield = "const"
#use_Efield = "lin"
#use_Efield = "coupled"


#%%

## geometry
W = 4e-3 # m
radiusfactor = 2
thickness = 0.4e-4
LEN = 2e-3
RAD = radiusfactor*LEN

bodies = []

MID_y = np.sqrt(RAD*RAD-LEN*LEN)
angle_rad = np.arcsin(LEN/RAD)
angle = angle_rad/np.pi*180


edges_ab = Wire([Segment(Pnt(0, MID_y-RAD+thickness/2, -W/2), Pnt(0, MID_y-RAD+thickness/2, W/2)),
                Segment(Pnt(0, MID_y-RAD+thickness/2, W/2), Pnt(0, MID_y-RAD, W/2)),
                Segment(Pnt(0, MID_y-RAD, W/2), Pnt(0, MID_y-RAD, -W/2)),
                Segment(Pnt(0, MID_y-RAD, -W/2), Pnt(0, MID_y-RAD+thickness/2, -W/2))
        ])
bodies.append(Face(edges_ab).Revolve(Axis((0,MID_y,0),Z),angle))

bodies[-1].faces[5].name = "x1"
bodies[-1].faces[0].name = "electrode0"
bodies[-1].faces[2].name = "interior"
bodies[-1].faces[1].name = "z1"
bodies[-1].faces[3].name = "z0"

bodies.append(Face(edges_ab).Revolve(Axis((0,MID_y,0),Z),-angle))

bodies[-1].faces[5].name = "x0"
bodies[-1].faces[0].name = "electrode0"
bodies[-1].faces[2].name = "interior"
bodies[-1].faces[1].name = "z1"
bodies[-1].faces[3].name = "z0"



edges_ab = Wire([Segment(Pnt(0, MID_y-RAD, -W/2), Pnt(0, MID_y-RAD, W/2)),
                Segment(Pnt(0, MID_y-RAD, W/2), Pnt(0, MID_y-RAD-thickness/2, W/2)),
                Segment(Pnt(0, MID_y-RAD-thickness/2, W/2), Pnt(0, MID_y-RAD-thickness/2, -W/2)),
                Segment(Pnt(0, MID_y-RAD-thickness/2, -W/2), Pnt(0, MID_y-RAD, -W/2))
        ])
bodies.append(Face(edges_ab).Revolve(Axis((0,MID_y,0),Z),angle))
bodies[-1].faces[5].name = "x1"
bodies[-1].faces[0].name = "interior"
bodies[-1].faces[2].name = "electrode1"
bodies[-1].faces[1].name = "z1"
bodies[-1].faces[3].name = "z0"
for ed in bodies[-1].edges:
    if np.abs(ed.center[0]+LEN) < 1e-12: ed.name = "bbndx0"
for ed in bodies[-1].edges:
    if np.abs(ed.center[0]-LEN) < 1e-12: ed.name = "bbndx1"

bodies.append(Face(edges_ab).Revolve(Axis((0,MID_y,0),Z),-angle))
bodies[-1].faces[5].name = "x0"
bodies[-1].faces[0].name = "interior"
bodies[-1].faces[2].name = "electrode1"
bodies[-1].faces[1].name = "z1"
bodies[-1].faces[3].name = "z0"

for ed in bodies[-1].edges:
    if np.abs(ed.center[0]+LEN) < 1e-12: ed.name = "bbndx0"
for ed in bodies[-1].edges:
    if np.abs(ed.center[0]-LEN) < 1e-12: ed.name = "bbndx1"


bodies[-1].vertices[47].name = "point0"

geometry = Glue(bodies)
geo = OCCGeometry(geometry)
ngmesh = geo.GenerateMesh(maxh=W/25)


mesh = Mesh(ngmesh)
Draw(mesh)



#%%


order = 3
mesh.Curve(order)

mu = 1e-2*1e6
epsilon0 = 8.854e-12
epsilon = 5*epsilon0

Voltage = 40

BCel = "electrode."
BCel1 = "electrode1"
BCtip = "bnd_tip"

#%%
## definition of finite element spaces - Taylor-Hood displacement/pressure formulation
fes_u = VectorH1(mesh, order=order, dirichletx_bbnd="bbndx0", dirichlety_bbnd="bbndx.", dirichletz_bbbnd="point0")
fes_phi = H1(mesh, order=order, dirichlet=BCel)
fes_pres = H1(mesh, order=order-1)


fes  = fes_u*fes_phi*fes_pres

solution = GridFunction(fes)

#%%
## Neo-Hooke potential for displacement-pressure formulation
def NeoHooke(C, detF, pres):
    return 0.5*mu*(Trace(C) - mesh.dim)- mu*log(detF) + pres*log(detF)

#%%
## pre-compute electric field variants
# constant E: use V/t*pol
# uncoupled E: use -V*Grad(phi)

phi0 = GridFunction(fes_phi)
phi0.Set(1, definedon=mesh.Boundaries(BCel1))

ahelp = BilinearForm(fes_phi, condense=True)
phi0_ = fes_phi.TrialFunction()
ahelp += SymbolicEnergy(Grad(phi0_)*Grad(phi0_))
solvers.Newton(ahelp, phi0)
pol = 1/Norm(Grad(phi0))*Grad(phi0)
par = Parameter(0)
#%%
## define symbolic energies
u_, phi_, pres_ = fes.TrialFunction()

bfA = BilinearForm(fes, symmetric=True, condense=True, printelmat=False)

F_ = Id(mesh.dim) + Grad(u_)
C_ = F_.trans*F_
strain_ = 0.5*(C_-Id(mesh.dim))
if use_Efield == "coupled":
    Efield_ = -grad(phi_)
elif use_Efield == "const":
    Efield_ = par*Voltage/thickness*pol
elif use_Efield == "lin":
    Efield_ = -par*Voltage*Grad(phi0)


bfA += Variation( NeoHooke(C_,Det(F_),pres_)*dx).Compile()
bfA += Variation( -0.5*epsilon*InnerProduct(Efield_,Inv(C_)*Efield_)*dx ).Compile()

# dummy if electric field is precomputed
if use_Efield != "coupled":
    bfA += Variation(phi_*phi_*dx)

#%%
## visualization of fields
u, phi, pres = solution.components

Draw(u, mesh, "disp", deformation=solution.components[0])
F = Id(mesh.dim) + Grad(u)
E = 0.5*(F.trans*F - Id(mesh.dim))
Draw(BoundaryFromVolumeCF(E), mesh, "strain", deformation=solution.components[0])
if use_Efield == "coupled":
    Draw(phi, mesh, "phi")
    Draw(BoundaryFromVolumeCF(-Grad(phi)), mesh, "Efield", deformation=solution.components[0])

#%%
## loadstepping
loadsteps = np.linspace(0,9,19)

last_loadfac = loadsteps[0]
solution.vec[:] = 0


for loadfac in loadsteps[1:]:
    print(f"loadstep {loadfac}")

    solution.components[1].vec.data += Voltage*(loadfac-last_loadfac)*phi0.vec
    par.Set(loadfac)


    last_loadfac = loadfac

    with TaskManager(): 
        res, numit = NewtonWithLinesearch(bfA, solution.vec, inverse="pardiso", relerror=1e-8,  abserror=1e-10, min_damping=1e-6, maxnewton=20)
    print(res, numit)

    if res != 0:
        break



    int1 = Integrate(1, mesh, definedon=mesh.BBoundaries("bbndx1"))

    ux = 1/int1*Integrate(solution.components[0], mesh, definedon=mesh.BBoundaries("bbndx1"))
    uy = solution.components[0](mesh(0,MID_y-RAD,0))

    print(radiusfactor, Voltage*par.Get(), ux[0], ux[1], ux[2], uy[0], uy[1], uy[2])

    # outfile_dist = open(filename+f"{maxV*par.Get()}V_distribution.dat", "w")
    # alphalist = np.linspace(-angle_rad,angle_rad,101)
    # pointlist = np.vstack((RAD*np.sin(alphalist), MID_y-RAD*np.cos(alphalist), np.zeros(alphalist.shape)))
    # for i in range(alphalist.shape[0]):
    #     ai = alphalist[i]
    #     p = pointlist[:,i]
    #     disppoint = solution.components[0](mesh(p[0],p[1],p[2]))
    #     print(f"{ai}\t{p[0]}\t{p[1]}\t{p[2]}\t{disppoint[0]}\t{disppoint[1]}\t{disppoint[2]}", file=outfile_dist)
    # outfile_dist.flush()
    # outfile_dist.close()


    Redraw()



# %%
