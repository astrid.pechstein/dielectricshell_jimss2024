#%%
import netgen.gui
from ngsolve import *

import numpy as np

from netgen.occ import *

from newtonmethod import NewtonWithLinesearch

SetNumThreads(4)
#%%

## choose one
use_Efield = "const"
use_Efield = "lin"
use_Efield = "coupled"

#%%
W = 4e-3 ## m
radiusfactor = 2
thickness = 0.4e-4
LEN = 2e-3
RAD = radiusfactor*LEN
angle = np.arcsin(LEN/RAD)/np.pi*180*2
print("angle", angle)

MID_y = np.sqrt(RAD*RAD-LEN*LEN)
edge_straight = Segment(Pnt(-LEN,0, -W/2), Pnt(-LEN,0, W/2))
face = edge_straight.Revolve(Axis((0,MID_y,0),Z),angle)


face.edges[0].name = "z0"
face.edges[2].name = "x0"
face.edges[3].name = "x1"
face.edges[1].name = "z1"

face.vertices[4].name = "point0"


geometry = Glue([face])
geo = OCCGeometry(geometry)

ngmesh = geo.GenerateMesh(maxh=W/5)


mesh = Mesh(ngmesh)
Draw(mesh)



#%%

bbc_clamp = ""
bbc_x = "x0"
bbc_y = "x."
bbc_z = ""


thickness_order = 2

order = 2
mesh.Curve(order)

mu = 1e-2*1e6
epsilon0 = 8.854e-12
epsilon = 5*epsilon0
capacity = epsilon/(thickness)

Voltage = 40

fes_mom = HDivDivSurface(mesh, order=order-1, discontinuous=True)
fes_u = VectorH1(mesh, order=order, dirichletx_bbnd=bbc_x, dirichlety_bbnd=bbc_y, dirichletz_bbnd=bbc_z, dirichletz_bbbnd="point0")
fes_hyb = HDivSurface(mesh, order=order-1, orderinner=0, dirichlet_bbnd=bbc_clamp)

fes_curlcurl = HCurlCurl(mesh, order=order-1, discontinuous=True)
fes_l2 = SurfaceL2(mesh, order=order-1)

fes  = fes_u*fes_hyb*fes_curlcurl*fes_curlcurl*fes_curlcurl*fes_mom*fes_l2**thickness_order*fes_l2**thickness_order*fes_l2


print("dofs", sum(fes.FreeDofs()))
print("dofs coupling", sum(fes.FreeDofs(True)))

#%%

par = Parameter(0.0)
par.Set(0)

Efield = (par*Voltage)/(thickness)

# %%

solution = GridFunction(fes, name="solution")
solution0 = GridFunction(fes, name="solution0")
u = solution.components[0]
eps = solution.components[2]
kappa = solution.components[4]
mom = solution.components[5]


Nsurf = specialcf.normal(mesh.dim) # surface normal N
t     = specialcf.tangential(mesh.dim)
nel   = Cross(Nsurf, t) # in-plane edge normal

# first metric tensor
A    = Id(mesh.dim) - OuterProduct(Nsurf,Nsurf)

cfnphys = Normalize(Cof(A+Grad(u))*Nsurf)




gradN = specialcf.Weingarten(3) 

#%%
## averaged surface normal, see Neunteufel Schoeberl 2019
fesVF = VectorFacetSurface(mesh, order=order)
averednv = GridFunction(fesVF)
averednv_start = GridFunction(fesVF)

n_ = fesVF.TrialFunction()
n_.Reshape((3,))
bfF = BilinearForm(fesVF, symmetric=True)
bfF += Variation( (0.5*n_*n_ - (cfnphys)*n_)*ds(element_boundary=True))

def ComputeAveredNV(averednv):
    rf = averednv.vec.CreateVector()
    bfF.Apply(averednv.vec, rf)
    bfF.AssembleLinearization(averednv.vec)
    invF = bfF.mat.Inverse(fesVF.FreeDofs())
    averednv.vec.data -= invF*rf

ComputeAveredNV(averednv)
ComputeAveredNV(averednv_start)

cfn  = Normalize(CoefficientFunction( averednv.components ))
cfnR = Normalize(CoefficientFunction( averednv_start.components ))


#%%
## computation of second metric tensor B
B0 = GridFunction(fes_curlcurl)
Draw(B0, mesh, "B")
def ComputeB(B0):
    # for precomputing B0
    fes3B0 = HDivSurface(mesh, order=order-1, orderinner=0, dirichlet_bbnd=".*")
    fes_B0  = fes3B0*fes_curlcurl*fes_mom
    sol = GridFunction(fes_B0)

    hyb_, B_, mom_ = fes_B0.TrialFunction()
    hyb_, B_, mom_ = hyb_.Trace(), B_.Trace(), mom_.Trace()

    bfA0 = BilinearForm(fes_B0, symmetric=True)
    bfA0 += Variation( InnerProduct(B_, B_)*ds).Compile()
    bfA0 += Variation( (-InnerProduct(mom_, B_ + gradN))*ds ).Compile()
    bfA0 += Variation( (acos(nel*cfnR)-np.pi/2-hyb_*nel)*(mom_*nel)*nel*ds(element_boundary=True ) ).Compile()

    sol.vec[:] = 0
    NewtonWithLinesearch(bfA0, sol.vec, inverse="pardiso")
        
    B0.vec.data = sol.components[1].vec

ComputeB(B0)

#%%

u_, hyb_, eps_, R_, kappa_, mom_ = fes.TrialFunction()[:6]
mom_, hyb_, eps_, R_, kappa_ = mom_.Trace(), hyb_.Trace(), eps_.Trace(), R_.Operator("dualbnd"), kappa_.Trace()
lam_, pres_, E1_ = fes.TrialFunction()[6:]
lam_, pres_, E1_ = lam_.Trace(), pres_.Trace(), E1_.Trace()


Fsurf_    = grad(u_).Trace() + A
Csurf_    = Fsurf_.trans*Fsurf_
epssurf_ = 0.5*(Csurf_ - A)

nphys   = Normalize(Cof(Fsurf_)*Nsurf) # normal of deformed surface
tphys   = Normalize(Fsurf_*t)
nelphys = Cross(nphys,tphys) # in-plane edge normal of deformed surface

Hn_ = CoefficientFunction( (u_.Operator("hesseboundary").trans*nphys), dims=(3,3) )
pnaverage = Normalize( cfn - (tphys*cfn)*tphys )

#%%
gausspoints3 = [(-np.sqrt(3/5), 5/9), (0,8/9), ( np.sqrt(3/5), 5/9)]
gausspoints2 = [(-np.sqrt(1/3), 1), ( np.sqrt(1/3), 1)]
gausspoints1 = [(0,2)]
gausspoints4 = [(-np.sqrt(3/7+2/7*np.sqrt(6/5)), (18-np.sqrt(30))/36 ), 
               (-np.sqrt(3/7-2/7*np.sqrt(6/5)), (18+np.sqrt(30))/36 ), 
               (np.sqrt(3/7-2/7*np.sqrt(6/5)), (18+np.sqrt(30))/36 ), 
               (np.sqrt(3/7+2/7*np.sqrt(6/5)), (18-np.sqrt(30))/36 )  ]

bfA = BilinearForm(fes, symmetric=True, condense=False, printelmat=False)

gausspoints = gausspoints4



for (zi, wi) in gausspoints:
    zeta = thickness/2*zi
    weightdet = wi*thickness/2
    if thickness_order == 1:
        lamz = (lam_+1)*zeta 
        lamzp = (lam_+1)
        presz = pres_
    else:
        lamz = (lam_[0]+1)*zeta
        lamzp = lam_[0]+1
        presz = pres_[0]
        ifact = 1
        for i in range(2,thickness_order+1):
            lamzp += zeta**(i-1)/ifact*lam_[i-1]
            presz += zeta**(i-1)/ifact*pres_[i-1]
            ifact *= i
            lamz += zeta**(i)/ifact*lam_[i-1]

    C_ = A + 2*(eps_ + lamz*kappa_ - lamz*B0 + zeta*B0 + zeta*eps_*B0 + zeta*B0*eps_)

    I_C = Trace(C_)
    CNN = lamzp**2
    III_C2 = (Cof(C_)*Nsurf)*Nsurf
    FB = (-zeta*B0) + A
    III_Lambda = (Cof(FB)*Nsurf)*Nsurf

    NeoHooke = 0.5*mu*(I_C + CNN - 3) - mu/2*log(III_C2*CNN)
    bfA += Variation((NeoHooke)*weightdet*III_Lambda*ds).Compile()
    bfA += Variation( (III_C2*CNN-1) * presz *III_Lambda*weightdet*ds)

    if use_Efield == "const":
        Efield_r = -par*Voltage/thickness
        bfA += Variation(-0.5*epsilon * Efield_r**2/CNN*III_Lambda*weightdet*ds).Compile()
    elif use_Efield == "lin":
        Efield_r = -par*Voltage/thickness
        bfA += Variation(-0.5*epsilon * Efield_r**2/III_Lambda/CNN*weightdet*ds).Compile()
    elif use_Efield == "coupled":
        Efield_r = -par*Voltage/thickness+E1_*zeta
        bfA += Variation(-0.5*epsilon * Efield_r**2*III_Lambda/CNN*weightdet*ds).Compile()



bfA += Variation( (-InnerProduct(mom_, kappa_ + Hn_ + (1-nphys*Nsurf)*gradN))*ds ).Compile()
bfA += Variation( InnerProduct(eps_-epssurf_, R_)*ds(element_vb=BND ) )
bfA += Variation( InnerProduct(eps_-epssurf_, R_)*ds(element_vb=VOL ) )
bfA += Variation( (acos(nel*cfnR)-acos(nelphys*pnaverage)-hyb_*nel)*(mom_*nel)*nel*ds(element_boundary=True ) ).Compile()

#%%
## visualize fields
Draw(u, mesh, "disp", deformation=u)
Draw(mom, mesh, "moment")
Draw(eps, mesh, "strain")
Draw(kappa, mesh, "curv")

#%%
loadsteps = np.linspace(0,9,19)

solution.vec[:] = 0

for loadfac in loadsteps[1:]:
    print(f"loadstep {loadfac}")
    solution0.vec.data = solution.vec


    last_loadfac = loadfac
    
    par.Set(loadfac)

    res, numit = NewtonWithLinesearch(bfA, solution.vec)

    if res != 0: 
        break

    Redraw()

    int1 = Integrate(1, mesh, definedon=mesh.BBoundaries("x1"))

    ux = 1/int1*Integrate(solution.components[0], mesh, definedon=mesh.BBoundaries("x1"))
    uy = solution.components[0](mesh(0,-(RAD-MID_y),0,BND))

    print(radiusfactor, Voltage*par.Get(), ux[0], ux[1], ux[2], uy[0], uy[1], uy[2])


#%%


