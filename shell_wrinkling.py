#%%
import netgen.gui
#%%
from ngsolve import *

import numpy as np
from netgen.csg import *

from netgen.meshing import MeshingParameters

from newtonmethod import NewtonWithLinesearch

SetNumThreads(6)


#%%

## choose one
material = {"id": 9473, "rho": 1050, "C10": 9.00e3, "C01": 17.81e3, "thickness": 0.25e-3}
material = {"id": 4905, "rho": 960, "C10": 8.96e3, "C01": 19.05e3, "thickness": 0.5e-3}
# material = {"id": 4910, "rho": 960, "C10": 6.83e3, "C01": 19.27e3, "thickness": 1.0e-3}

thickness = material["thickness"]

Voltage = 1000
epsilon0 = 8.854e-12
epsilon = 4.12e-11
capacity = epsilon/(thickness)
charge = Voltage/(thickness)*capacity

order = 3



LEN = 100e-3
WID = 100e-3

stretchx = 1.5
stretchy = 1/np.sqrt(1.5)

#%%
## geometry

geo          = CSGeometry()
shell       = Plane(Pnt(0,0,0), Vec(0,0,1))

maxh_edge = LEN/60
if material["id"] == 9473: maxh_edge = LEN/80

maxh_int = LEN/20
if material["id"] == 9473: maxh_int = LEN/40

planex0 = Plane(Pnt(0,0,0), Vec(-1,0,0)).maxh(maxh_edge)
planey0 = Plane(Pnt(0,0,0), Vec(0,-1,0)).maxh(maxh_edge)
planex1 = Plane(Pnt(LEN/2,0,0), Vec(1,0,0))
planey1 = Plane(Pnt(0,WID,0), Vec(0,1,0)).maxh(maxh_edge)

finiteshell = shell * planex0 * planex1 * planey0 * planey1

geo.AddSurface(shell, finiteshell.bc("surface"))
geo.NameEdge(shell,planey0, "bottom")
geo.NameEdge(shell,planey1, "top")
geo.NameEdge(shell,planex0, "left")
geo.NameEdge(shell,planex1, "right")

mp = MeshingParameters(maxh=maxh_int, grading=0.2)
mesh = Mesh(geo.GenerateMesh(mp))

Draw(mesh)
mesh.Curve(order)

#%%

BCclamp = ".*"

BCx = "right|left|bottom|top"
BCy = "left|bottom|top"
BCz = "left|bottom|top"


fes_mom = HDivDivSurface(mesh, order=order-1, discontinuous=True)
fes_u = VectorH1(mesh, order=order, dirichletx_bbnd=BCx, dirichlety_bbnd=BCy, dirichletz_bbnd=BCz)
fes_hyb = HDivSurface(mesh, order=order-1, orderinner=0, dirichlet_bbnd=BCclamp)
fes_curlcurl = HCurlCurl(mesh, order=order-1, discontinuous=True)
fes_l2 = SurfaceL2(mesh, order=order-1)

fes  = fes_u*fes_hyb*fes_curlcurl*fes_curlcurl*fes_curlcurl*fes_mom*fes_l2**2*fes_l2**2*fes_l2


par_stretch = Parameter(0)
par_weight = Parameter(0)
par = Parameter(0.0)


#%%

solution = GridFunction(fes, name="solution")
solution_0 = GridFunction(fes)
u = solution.components[0]
eps = solution.components[2]
kappa = solution.components[4]
mom = solution.components[5]


Nsurf = specialcf.normal(mesh.dim) # surface normal N
t     = specialcf.tangential(mesh.dim)
nel   = Cross(Nsurf, t) # in-plane edge normal

gradN = specialcf.Weingarten(3) 


A = Id(mesh.dim) - OuterProduct(Nsurf,Nsurf)

cfnphys = Normalize(Cof(A+Grad(u))*Nsurf)

Fstretch = CoefficientFunction((1+par_stretch*(stretchx-1), 0, 0,
                                0, 1+par_stretch*(stretchy-1), 0,
                                0, 0, 1), dims=(3,3))
Estretch = CoefficientFunction((par_stretch*(stretchx-1), 0, 0,
                                0, par_stretch*(stretchy-1), 0,
                                0, 0, 0), dims=(3,3))


#%%
## averaged surface normal, see Neunteufel Schoeberl 2019
fesVF = VectorFacetSurface(mesh, order=order)
averednv = GridFunction(fesVF)
averednv_start = GridFunction(fesVF)

fesF = FacetSurface(mesh, order=0)
gfclamped = GridFunction(fesF)
gfclamped.Set(1,definedon=mesh.BBoundaries(BCclamp))

n_ = fesVF.TrialFunction()
n_.Reshape((3,))
bfF = BilinearForm(fesVF, symmetric=True)
bfF += Variation( (0.5*n_*n_ - ((1-gfclamped)*cfnphys+gfclamped*Nsurf)*n_)*ds(element_boundary=True))

def ComputeAveredNV(averednv):
    rf = averednv.vec.CreateVector()
    bfF.Apply(averednv.vec, rf)
    bfF.AssembleLinearization(averednv.vec)
    invF = bfF.mat.Inverse(fesVF.FreeDofs())
    averednv.vec.data -= invF*rf

ComputeAveredNV(averednv)
ComputeAveredNV(averednv_start)

cfn  = Normalize(CoefficientFunction( averednv.components ))
cfnR = Normalize(CoefficientFunction( averednv_start.components ))

#%%
## computation of second metric tensor B
B0 = GridFunction(fes_curlcurl)
Draw(B0, mesh, "B")
def ComputeB(B0):
    # for precomputing B0
    fes3B0 = HDivSurface(mesh, order=order-1, orderinner=0, dirichlet_bbnd=".*")
    fes_B0  = fes3B0*fes_curlcurl*fes_mom
    sol = GridFunction(fes_B0)

    hyb_, B_, mom_ = fes_B0.TrialFunction()
    hyb_, B_, mom_ = hyb_.Trace(), B_.Trace(), mom_.Trace()

    bfA0 = BilinearForm(fes_B0, symmetric=True)
    bfA0 += Variation( InnerProduct(B_, B_)*ds).Compile()
    bfA0 += Variation( (-InnerProduct(mom_, B_ + gradN))*ds ).Compile()
    bfA0 += Variation( (acos(nel*cfnR)-np.pi/2-hyb_*nel)*(mom_*nel)*nel*ds(element_boundary=True ) ).Compile()

    sol.vec[:] = 0
    NewtonWithLinesearch(bfA0, sol.vec, inverse="pardiso")
        
    B0.vec.data = sol.components[1].vec

ComputeB(B0)

#%%


u_, hyb_, eps_, R_, kappa_, mom_ = fes.TrialFunction()[:6]
mom_, hyb_, eps_, R_, kappa_ = mom_.Trace(), hyb_.Trace(), eps_.Trace(), R_.Operator("dualbnd"), kappa_.Trace()
lam_, pres_, E1_ = fes.TrialFunction()[6:]
lam_, pres_, E1_ = lam_.Trace(), pres_.Trace(), E1_.Trace()


Ftilde_    = Grad(u_).Trace() + A
Ctilde_    = Ftilde_.trans*Ftilde_
epssurf_tilde_ = 0.5*(Ctilde_ - A)


nphys   = Normalize(Cof(Ftilde_)*Nsurf) # normal of deformed surface
tphys   = Normalize(Ftilde_*t)
nelphys = Cross(nphys,tphys) # in-plane edge normal of deformed surface

Hn_ = CoefficientFunction( (u_.Operator("hesseboundary").trans*nphys), dims=(3,3) )
pnaverage = Normalize( cfn - (tphys*cfn)*tphys )

#%%
bfA = BilinearForm(fes, symmetric=True, condense=True)
gausspoints = [(-np.sqrt(3/5), 5/9), (0,8/9), ( np.sqrt(3/5), 5/9)]

for (zi, wi) in gausspoints:
    zeta = zi*thickness/2
    weightdet = wi*thickness/2

    lamz = (lam_[0]+1)*zeta + lam_[1]/2*zeta*zeta
    lamzp = lam_[0]+1 + lam_[1]*zeta
    presz = pres_[0] + pres_[1]*zeta

    C_ = Fstretch.trans*(2*(eps_ + lamz*kappa_) + A )*Fstretch

    I_C = Trace(C_)
    III_C2 = (Cof(C_)*Nsurf)*Nsurf 
    CNN = lamzp**2
    detC = III_C2*CNN

    I_barC = detC**(-1/3)*(I_C + CNN)
    II_barC = 0.5*detC**(-2/3)*((I_C + CNN)**2 - (InnerProduct(C_,C_) + CNN*CNN))
    
    MooneyRivlin1 = material["C10"]*(I_barC - 3) 
    MooneyRivlin2 = material["C01"]*(II_barC - 3) 

    bfA += Variation(weightdet*(MooneyRivlin1)*ds).Compile()
    bfA += Variation(weightdet*(MooneyRivlin2)*ds).Compile()
    bfA += Variation( (detC-1) * presz *weightdet*ds).Compile()

    Efield = par*Voltage/thickness + E1_*zeta
    bfA += Variation(-0.5*epsilon * Efield**2 * weightdet/CNN*ds).Compile()

bfA += Variation( (InnerProduct(mom_, kappa_ + Hn_ + (1-nphys*Nsurf)*gradN))*ds ).Compile()
bfA += Variation( InnerProduct(eps_-epssurf_tilde_, R_)*ds(element_vb=BND) )
bfA += Variation( InnerProduct(eps_-epssurf_tilde_, R_)*ds(element_vb=VOL) )
mom_nn = InnerProduct(mom_*nel, nel)
bfA += Variation( -(acos(nel*cfnR)-acos(nelphys*pnaverage)-hyb_*nel)*mom_nn*ds(element_boundary=True) ).Compile()

bfA += Variation( 1/(stretchx*stretchy)*thickness*material["rho"]*par_weight*9.81*u_[2]*ds)


#%%

ndof_coupling = sum(fes.FreeDofs(True))

Draw(u, mesh, "disp", deformation=u)
Draw(eps, mesh, "eps", deformation=u)
Draw(mom, mesh, "mom", deformation=u)

Draw(u[2], mesh, "uz", deformation=u)




#%%



for loadpar in [1]: # [0,0.1,1]:# in range(0,numsteps):
    solution_0.vec.data = solution.vec

    par_weight.Set(loadpar)
    par.Set(0)
    par_stretch.Set(0)
    print("Loadparameter gravity: ", par_weight.Get())
    print("Loadparameter stretch: ", par_stretch.Get())
    print("Loadparameter voltage: ", par.Get())


    with TaskManager():
        (res,numit) = NewtonWithLinesearch(a=bfA, x=solution.vec, inverse="pardiso", factor=1.03)

    Redraw()

#%%
    
glist2 = np.linspace(0,1,11)
for loadpar in glist2[1:]:
    solution_0.vec.data = solution.vec

    par_stretch.Set(loadpar)
    par.Set(0)
    print("Loadparameter gravity: ", par_weight.Get())
    print("Loadparameter stretch: ", par_stretch.Get())
    print("Loadparameter voltage: ", par.Get())

    with TaskManager():
        (res,numit) = NewtonWithLinesearch(a=bfA, x=solution.vec, inverse="pardiso", factor=1.03)

    Redraw()



#%%
def doStep(loadpar, solution, maxit=200):
    par.Set(loadpar)
    print("Loadparameter gravity: ", par_weight.Get())
    print("Loadparameter stretch: ", par_stretch.Get())
    print("Loadparameter voltage: ", par.Get())

    with TaskManager():
        (res,numit) = NewtonWithLinesearch(a=bfA, x=solution.vec, inverse="pardiso", abserror=1e-6, maxnewton=maxit, factor=1.03, min_damping=1e-4)

    if res: 
        return 1
    
    eval_uz_line = [solution.components[0](mesh(LEN/2,ix/100*WID,0, BND))[2] for ix in range(101)]
    maxuz = max((eval_uz_line))
    minuz = min((eval_uz_line))
    maxuz1 = max((eval_uz_line[5:95]))
    minuz1 = min((eval_uz_line[5:95]))



    string = f"{loadpar}\t{Voltage*loadpar}\t{minuz}\t{maxuz}\t{minuz1}\t{maxuz1}"
    print(string)


    # for cmp in range(11):
    #     if np.abs(gg-cmp) < 1e-14:
    #         for u in eval_uz_line:
    #             print(u, end="\t",file=outfile)
    #         print("\n", end="", file=outfile, flush=True)
    #         outfile.flush()

    return 0

#%%

if material["id"] == 4910:
    glist = (np.linspace(0,10,41, endpoint=True))
    
if material["id"] == 9473:
    glist = np.concatenate((np.linspace(0,0.7,7, endpoint=False),
                            np.linspace(0.7,0.8,4,endpoint=False),
                            np.linspace(0.8,2.2,14,endpoint=False),
                            np.linspace(2.2,4,19)))

if material["id"] == 4905:
    glist = np.concatenate((np.linspace(0,2,8, endpoint=False),
                            np.linspace(2,3,16,endpoint=False),
                            np.linspace(3,7,4*4,endpoint=False),
                            np.linspace(7,10,3*8+1)))


#%%
     
for loadpar in glist:# in range(0,numsteps):
    solution_0.vec.data = solution.vec
    ComputeAveredNV(averednv)

    err = doStep(loadpar, solution)

    if err: 
        break

    Redraw()

#%%