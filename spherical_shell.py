#%%
import netgen.gui
from ngsolve import *
import numpy as np
from netgen.csg import *

from newtonmethod import NewtonWithLinesearch

SetNumThreads(4)
#%%




#%%
    
thickness = 0.5
RAD = 20
mu = 1e-2


Voltage = 5000
epsilon0 = 8.854e-12
epsilon = 5*epsilon0
N_chain = 5
capacity = epsilon/(thickness)
charge = Voltage/(thickness)*capacity

order = 2


BCclamp = "sym.|bottom"
geo          = CSGeometry()
sphere       = Sphere(Pnt(0,0,0), RAD)
bot          = Plane(Pnt(0,0,0), Vec(0,0,-1)).maxh(RAD*np.sin(np.pi/2/50))
bot1          = Plane(Pnt(0,0,RAD*np.sin(np.pi/2/60)), Vec(0,0,-1)).maxh(RAD*np.sin(np.pi/2/50))
bot2          = Plane(Pnt(0,0,RAD*np.sin(np.pi/2/30)), Vec(0,0,-1)).maxh(RAD*np.sin(np.pi/2/50))
bot3          = Plane(Pnt(0,0,RAD*np.sin(np.pi/2/18)), Vec(0,0,-1)).maxh(RAD*np.sin(np.pi/2/50))
bot4          = Plane(Pnt(0,0,RAD*np.sin(np.pi/2/12)), Vec(0,0,-1)).maxh(RAD*np.sin(np.pi/2/50))
# top          = Plane(Pnt(0,0,W), Vec(0,0,1))
planex = Plane(Pnt(0,0,0), Vec(-1,0,0))
planey = Plane(Pnt(0,0,0), Vec(0,-1,0))
finitesphere1 = sphere * bot * planex * planey - bot1
finitesphere2 = sphere * bot1 * planex * planey - bot2
finitesphere3 = sphere * bot2 * planex * planey - bot3
finitesphere4 = sphere * bot3 * planex * planey - bot4
finitesphere5 = sphere * bot4 * planex * planey


geo.AddSurface(sphere, finitesphere1.bc("surface"))
geo.AddSurface(sphere, finitesphere2.bc("surface"))
geo.AddSurface(sphere, finitesphere3.bc("surface"))
geo.AddSurface(sphere, finitesphere4.bc("surface"))
geo.AddSurface(sphere, finitesphere5.bc("surface"))
geo.NameEdge(sphere,bot, "bottom")
geo.NameEdge(sphere,planex, "symx")
geo.NameEdge(sphere,planey, "symy")

geo.AddPoint(Pnt(RAD,0,0), "pntload_x")
geo.AddPoint(Pnt(0,RAD,0), "pntload_y")


from netgen.meshing import MeshingParameters
maxhrel = 9
mp = MeshingParameters(maxh=RAD/maxhrel, grading=0.1)
mesh = Mesh(geo.GenerateMesh(mp))

Draw(mesh)

mesh.Curve(order)

#%%
fes_mom = HDivDivSurface(mesh, order=order-1, discontinuous=True)
fes_u = VectorH1(mesh, order=order, dirichletx_bbnd="symx|bottom", dirichlety_bbnd="symy|bottom", dirichletz_bbnd="bottom")
fes_hyb = HDivSurface(mesh, order=order-1, orderinner=0, dirichlet_bbnd=BCclamp)
fes_curlcurl = HCurlCurl(mesh, order=order-1, discontinuous=True, print=True)
fes_l2 = SurfaceL2(mesh, order=order-1)

fes  = fes_u*fes_hyb*fes_curlcurl*fes_curlcurl*fes_curlcurl*fes_mom*fes_l2**2 * fes_l2**2 * fes_l2

#%%
par = Parameter(0.0)

solution = GridFunction(fes, name="solution")
solution_0 = GridFunction(fes)
u = solution.components[0]
eps = solution.components[2]
kappa = solution.components[4]
mom = solution.components[5]


Nsurf = specialcf.normal(mesh.dim) # surface normal N
t     = specialcf.tangential(mesh.dim)
nel   = Cross(Nsurf, t) # in-plane edge normal

gradN = specialcf.Weingarten(3) 


A = Id(mesh.dim) - OuterProduct(Nsurf,Nsurf)

cfnphys = Normalize(Cof(A+Grad(u))*Nsurf)

#%%

fesVF = VectorFacetSurface(mesh, order=order)
averednv = GridFunction(fesVF)
averednv_start = GridFunction(fesVF)


fesF = FacetSurface(mesh, order=0)
gfclamped = GridFunction(fesF)
gfclamped.Set(1,definedon=mesh.BBoundaries(BCclamp))

n_ = fesVF.TrialFunction()
n_.Reshape((3,))
bfF = BilinearForm(fesVF, symmetric=True)
bfF += Variation( (0.5*n_*n_ - ((1-gfclamped)*cfnphys+gfclamped*Nsurf)*n_)*ds(element_boundary=True))


def ComputeAveredNV(averednv):
    rf = averednv.vec.CreateVector()
    bfF.Apply(averednv.vec, rf)
    bfF.AssembleLinearization(averednv.vec)
    invF = bfF.mat.Inverse(fesVF.FreeDofs(), inverse="pardiso")
    averednv.vec.data -= invF*rf


ComputeAveredNV(averednv)
ComputeAveredNV(averednv_start)

cfn  = Normalize(CoefficientFunction( averednv.components ))
cfnR = Normalize(CoefficientFunction( averednv_start.components ))

#%%
B0 = GridFunction(fes_curlcurl)
Draw(B0, mesh, "B")
def ComputeB(B0):
    # for precomputing B0
    fes3B0 = HDivSurface(mesh, order=order-1, orderinner=0, dirichlet_bbnd=".*")
    fes_B0  = fes3B0*fes_curlcurl*fes_mom
    sol = GridFunction(fes_B0)

    hyb_, B_, mom_ = fes_B0.TrialFunction()
    hyb_, B_, mom_ = hyb_.Trace(), B_.Trace(), mom_.Trace()

    bfA0 = BilinearForm(fes_B0, symmetric=True)
    bfA0 += Variation( InnerProduct(B_, B_)*ds).Compile()
    bfA0 += Variation( (-InnerProduct(mom_, B_ + gradN))*ds ).Compile()
    bfA0 += Variation( (acos(nel*cfnR)-np.pi/2-hyb_*nel)*(mom_*nel)*nel*ds(element_boundary=True ) ).Compile()

    sol.vec[:] = 0
    NewtonWithLinesearch(bfA0, sol.vec, inverse="pardiso")
        
    B0.vec.data = sol.components[1].vec

ComputeB(B0)

#%%

u_, hyb_, eps_, R_, kappa_, mom_ = fes.TrialFunction()[:6]
mom_, hyb_, eps_, R_, kappa_ = mom_.Trace(), hyb_.Trace(), eps_.Trace(), R_.Operator("dualbnd"), kappa_.Trace()
lam_, pres_, E1_ = fes.TrialFunction()[6:]
lam_, pres_, E1_ = lam_.Trace(), pres_.Trace(), E1_.Trace()

Fsurf_    = grad(u_).Trace() + A
Csurf_    = Fsurf_.trans*Fsurf_
epssurf_ = 0.5*(Csurf_ - A)

nphys   = Normalize(Cof(Fsurf_)*Nsurf) #  normal of deformed surface
tphys   = Normalize(Fsurf_*t)
nelphys = Cross(nphys,tphys) # in-plane edge normal of deformed surface

Hn_ = CoefficientFunction( (u_.Operator("hesseboundary").trans*nphys), dims=(3,3) )
pnaverage = Normalize( cfn - (tphys*cfn)*tphys )


bfA = BilinearForm(fes, symmetric=True, condense=True)
gausspoints = [(-np.sqrt(3/5), 5/9), (0,8/9), ( np.sqrt(3/5), 5/9)]

for (zi, wi) in gausspoints:
    zeta = thickness/2*zi
    weightdet = wi*thickness/2
    lamz = (lam_[0]+1)*zeta + lam_[1]/2*zeta*zeta
    lamzp = lam_[0]+1 + lam_[1]*zeta
    presz = pres_[0] + pres_[1]*zeta

    C_ = A + 2*(eps_ + lamz*kappa_ - lamz*B0 + zeta*B0 + zeta*eps_*B0 + zeta*B0*eps_)

    I_C = Trace(C_)
    CNN = lamzp**2
    III_C2 = (Cof(C_)*Nsurf)*Nsurf
    FB = (-zeta*B0) + A
    III_FB = (Cof(FB)*Nsurf)*Nsurf

    detC = III_C2*CNN
    I_barC = detC**(-1/3)*(I_C + CNN)
    
    ArrudaBoyce = 0.5*mu*(I_barC - 3) + mu/20/N_chain*(I_barC**2 - 9) + \
        11*mu/1050/N_chain**2*(I_barC**3 - 3**3) + \
        19*mu/7000/N_chain**3*(I_barC**4 - 3**4) + \
        519*mu/673750/N_chain**4*(I_barC**5 - 3**5)
    bfA += Variation(weightdet*III_FB*(ArrudaBoyce)*ds).Compile()
    Efield_r = -par*Voltage/thickness + E1_*zeta
    bfA += Variation(-0.5*epsilon * Efield_r**2*III_FB/CNN*weightdet*ds).Compile()
    bfA += Variation( (sqrt(III_C2*CNN)-1) * presz *III_FB*weightdet*ds)


bfA += Variation( (InnerProduct(mom_, kappa_ + Hn_ + (1-nphys*Nsurf)*gradN))*ds ).Compile()
bfA += Variation( InnerProduct(eps_-epssurf_, R_)*ds(element_vb=BND) )
bfA += Variation( InnerProduct(eps_-epssurf_, R_)*ds(element_vb=VOL) )
bfA += Variation( -(acos(nel*cfnR)-acos(nelphys*pnaverage)-hyb_*nel)*(mom_*nel)*nel*ds(element_boundary=True) ).Compile()

#%%

ndof_coupling = sum(fes.FreeDofs(True))


Draw(u, mesh, "disp", deformation=u)
Draw(u*Nsurf, mesh, "dispr", deformation=u)

Draw(mom, mesh, "m")
Draw(kappa, mesh, "kappa")

#%%

glist = np.concatenate((np.linspace(0,0.8,4,endpoint=False),
                        np.linspace(0.8,1,4, endpoint=False),
                        np.linspace(1.0,1.03,3, endpoint=False),
                        np.linspace(1.03,1.05,5, endpoint=False),
                        np.linspace(1.05,1.06,20, endpoint=False),
                        np.linspace(1.06,1.065,11, endpoint=True)))



#%%
counter = 0

lastloadpar = glist[0]
with TaskManager():
    for loadpar in glist[1:]:
        solution_0.vec.data = solution.vec
        par.Set(loadpar)
        print("Loadstep value =", par.Get())
        print("Applied voltage =", Voltage*par.Get())

        # ComputeAveredNV(averednv)

        (err,numit) = NewtonWithLinesearch(a=bfA, x=solution.vec, inverse="pardiso", abserror=1e-7, maxnewton=40, factor=1.03)



        if err > 1e-3:
            print("Newton method did not converge")
            solution.vec.data = solution_0.vec
            par.Set(lastloadpar)
            break

        poslist = []

        for theta in np.linspace(0,np.pi/2,101):
            ur = u*Nsurf
            pos = ur(mesh(RAD*np.cos(np.pi/2/18)*np.cos(theta), RAD*np.cos(np.pi/2/18)*np.sin(theta), RAD*np.sin(np.pi/2/18), BND))
            poslist.append(pos)
            # print(pos, end="\t", file=outfile)

        print("min/max", np.min(poslist), np.max(poslist))

        Redraw()
        lastloadpar = loadpar



#%%